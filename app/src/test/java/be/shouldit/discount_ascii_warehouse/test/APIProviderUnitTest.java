package be.shouldit.discount_ascii_warehouse.test;

import android.os.Build;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import be.shouldit.discount_ascii_warehouse.data.Ascii;
import be.shouldit.discount_ascii_warehouse.data.api.AsciiProvider;
import retrofit2.Call;
import retrofit2.Response;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */

@RunWith(RobolectricTestRunner.class)
@Config(manifest = "src/main/AndroidManifest.xml", sdk = Build.VERSION_CODES.M)
public class APIProviderUnitTest {

    AsciiProvider asciiWarehouseAPI;
    final int MAX_RESULTS = 100;

    @Before
    public void setUp() {
        asciiWarehouseAPI = new AsciiProvider(RuntimeEnvironment.application);
    }

    @Test
    public void testGetFirstTenResultsFromAPI() throws IOException {
        int resultNum = 10;

        Call<List<Ascii>> call = asciiWarehouseAPI.searchAscii(resultNum, 0, null, false);
        Response<List<Ascii>> response = call.execute();
        List<Ascii> results = response.body();

        assertEquals(results.size(), 10);
    }

    @Test
    public void testGetQueryResults() throws IOException {

        AsciiAPIResult apiResult = searchAPI("nose", false);
        assertEquals("nose", apiResult.taggedAscii("nose"), 2);
        assertEquals("nose", apiResult.tagsOccurrences("nose"), 2);

        apiResult = searchAPI("nose", true);
        assertEquals("nose", apiResult.taggedAscii("nose"), 2);
        assertEquals("nose", apiResult.tagsOccurrences("nose"), 2);

        apiResult = searchAPI("happy", false);
        assertEquals("happy", apiResult.taggedAscii("happy"), 2);
        assertEquals("happy", apiResult.tagsOccurrences("happy"), 2);

        apiResult = searchAPI("happy", true);
        assertEquals("happy", apiResult.taggedAscii("happy"), 2);
        assertEquals("happy", apiResult.tagsOccurrences("happy"), 2);

        apiResult = searchAPI("adipiscing", false);
        assertEquals("adipiscing", apiResult.taggedAscii("adipiscing"), 15);
        assertEquals("adipiscing", apiResult.tagsOccurrences("adipiscing"), 15);

        apiResult = searchAPI("adipiscing", true);
        assertEquals("adipiscing", apiResult.taggedAscii("adipiscing"), 13);
        assertEquals("adipiscing", apiResult.tagsOccurrences("adipiscing"), 13);

        apiResult = searchAPI("amet", false);
        assertEquals("amet", apiResult.taggedAscii("amet"), 13);
        assertEquals("amet", apiResult.tagsOccurrences("amet"), 15);

        apiResult = searchAPI("amet", true);
        assertEquals("amet", apiResult.taggedAscii("amet"), 11);
        assertEquals("amet", apiResult.tagsOccurrences("amet"), 12);

        apiResult = searchAPI("zoidberg", false);
        assertEquals("zoidberg", apiResult.taggedAscii("zoidberg"), 1);
        assertEquals("zoidberg", apiResult.tagsOccurrences("zoidberg"), 1);

        apiResult = searchAPI("zoidberg", true);
        assertEquals("zoidberg", apiResult.taggedAscii("zoidberg"), 1);
        assertEquals("zoidberg", apiResult.tagsOccurrences("zoidberg"), 1);

        apiResult = searchAPI("consectetur", false);
        assertEquals("consectetur", apiResult.taggedAscii("consectetur"), 22);
        assertEquals("consectetur", apiResult.tagsOccurrences("consectetur"), 24);

        apiResult = searchAPI("consectetur", true);
        assertEquals("consectetur", apiResult.taggedAscii("consectetur"), 17);
        assertEquals("consectetur", apiResult.tagsOccurrences("consectetur"), 19);
    }

    @Test
    public void testConsumeAPI() throws IOException {

        AsciiAPIResult asciiAPIResult = consumeAPI("", false);

        System.out.format("Consumed API: %d ascii\n", asciiAPIResult.size());
        asciiAPIResult.dumpAsciiList();
        System.out.println("Tags list:");
        asciiAPIResult.dumpAsciiTagsOccurrences();

        assertEquals(asciiAPIResult.size(), 100);

        AsciiAPIResult asciiAPIResultInStock = consumeAPI("", true);

        System.out.format("Consumed API: %d ascii\n", asciiAPIResultInStock.size());
        asciiAPIResultInStock.dumpAsciiList();
        System.out.println("Tags list:");
        asciiAPIResultInStock.dumpAsciiTagsOccurrences();

        assertEquals(asciiAPIResultInStock.size(), 87);
    }

    @Test
    public void testOnlyInStock() throws IOException {

        AsciiAPIResult allResult = searchAPI("", false);
        allResult.dumpAsciiList();
        assertEquals(allResult.size(), 100);

        AsciiAPIResult onlyStockResult = searchAPI("", true);
        onlyStockResult.dumpAsciiList();
        assertEquals(onlyStockResult.size(), 87);
    }

    private AsciiAPIResult searchAPI(String query, boolean onlyInStock) throws IOException {

        return searchAPI(MAX_RESULTS, 0, query, onlyInStock);
    }

    private AsciiAPIResult searchAPI(int resultNum, int skip, String query, boolean onlyInStock) throws IOException {

        System.out.format("Querying API. Num: '%d', Skip: '%d', Query: '%s', Stock: '%b'\n",
                resultNum, skip, query, onlyInStock);

        AsciiAPIResult asciiApiResult = new AsciiAPIResult();

        Call<List<Ascii>> call = asciiWarehouseAPI.searchAscii(resultNum, skip, query, onlyInStock);
        Response<List<Ascii>> response = call.execute();

        if (response.body() != null) {

            List<Ascii> results = response.body();
            assertNotNull(results);
            asciiApiResult.addAll(results);
        }

        return asciiApiResult;
    }

    private AsciiAPIResult consumeAPI(String query, boolean onlyInStock) throws IOException {

        int resultNum = 10;
        int consumed = 0;

        AsciiAPIResult asciiApiResult = new AsciiAPIResult();
        boolean run = true;

        while (run) {

            AsciiAPIResult result = searchAPI(resultNum, consumed*resultNum, query, onlyInStock);
            consumed++;

            if (result.size() != 0) {
                asciiApiResult.addAll(result.asciiList);
            } else {
                run = false;
            }
        }

        return asciiApiResult;
    }

    private class AsciiAPIResult {
        List<Ascii> asciiList;
        Map<String, Integer> asciiTags;

        public AsciiAPIResult() {
            asciiList = new ArrayList<>();
            asciiTags = new HashMap<>();
        }

        public void addAll(Collection<? extends Ascii> asciis) {

            for (Ascii a : asciis){
                add(a);
            }
        }

        public void add(Ascii ascii) {

            if (!asciiList.contains(ascii)) {
                asciiList.add(ascii);
                resetTags();
            }
        }

        private void resetTags() {

            asciiTags = new HashMap<>();

            for (Ascii a : asciiList) {
                for (String tag : a.getTags()) {
                    if (asciiTags.containsKey(tag)) {
                        asciiTags.put(tag, asciiTags.get(tag) + 1);
                    } else {
                        asciiTags.put(tag, 1);
                    }
                }
            }
        }

        public int size() {
            return asciiList.size();
        }

        public int tagsOccurrences(String tag) {

            if (asciiTags.containsKey(tag))
                return asciiTags.get(tag);
            else
                return 0;
        }

        public int taggedAscii(String tag) {

            int result = 0;
            for (Ascii a : asciiList){
                if (a.getTags().contains(tag))
                    result++;
            }

            return result;
        }

        public void dumpAsciiList() {
            for (Ascii a : asciiList) {
                System.out.format("%s\n", a.toString());
            }
        }

        public void dumpAsciiTagsOccurrences() {
            for (Map.Entry<String, Integer> entry : asciiTags.entrySet()) {
                System.out.format("'%s': %d\n", entry.getKey(), entry.getValue());
            }
        }
    }
}
