package be.shouldit.discount_ascii_warehouse.data.api;

import com.google.gson.Gson;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * Created by Marco on 08/06/16.
 */
public class ND_JSON_ConverterFactory extends Converter.Factory {

    private final Gson gson;

    private ND_JSON_ConverterFactory(Gson gson) {
        if (gson == null) throw new NullPointerException("gson == null");
        this.gson = gson;
    }

    public static ND_JSON_ConverterFactory create() {
        return create(new Gson());
    }

    public static ND_JSON_ConverterFactory create(Gson gson) {
        return new ND_JSON_ConverterFactory(gson);
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        return new ND_JSON_Converter<>(gson, type);
    }
}