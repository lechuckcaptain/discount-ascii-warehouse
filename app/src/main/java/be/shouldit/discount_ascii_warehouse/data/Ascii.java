package be.shouldit.discount_ascii_warehouse.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marco on 08/06/16.
 */
public class Ascii {

    private String type;
    private String id;
    private Integer size;
    private Integer price;
    private String face;
    private Integer stock;
    private List<String> tags = new ArrayList<>();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "Ascii{" +
                "type='" + type + '\'' +
                ", id='" + id + '\'' +
                ", size=" + size +
                ", price=" + price +
                ", face='" + face + '\'' +
                ", stock=" + stock +
                ", tags=" + tags +
                '}';
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof Ascii)) return false;

        Ascii ascii = (Ascii) o;

        if (type != null ? !type.equals(ascii.type) : ascii.type != null) return false;
        if (id != null ? !id.equals(ascii.id) : ascii.id != null) return false;
        if (size != null ? !size.equals(ascii.size) : ascii.size != null) return false;
        if (price != null ? !price.equals(ascii.price) : ascii.price != null) return false;
        if (face != null ? !face.equals(ascii.face) : ascii.face != null) return false;
        if (stock != null ? !stock.equals(ascii.stock) : ascii.stock != null) return false;
        return tags != null ? tags.equals(ascii.tags) : ascii.tags == null;

    }
}
