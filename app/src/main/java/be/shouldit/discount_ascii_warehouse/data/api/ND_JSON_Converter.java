package be.shouldit.discount_ascii_warehouse.data.api;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.Scanner;

import okhttp3.ResponseBody;
import retrofit2.Converter;

/**
 * Created by Marco on 08/06/16.
 */
public class ND_JSON_Converter<T> implements Converter<ResponseBody, T> {

    private final Gson gson;
    private final Type type;

    public ND_JSON_Converter(Gson gson, Type type) {
        this.gson = gson;
        this.type = type;
    }

    public static String convertToJsonArray(String ndjsonResponseBody) throws IOException {

        // replace new line delimiters with comma delimiters and remove last unneeded comma
        String jsonResponseBodyString = ndjsonResponseBody.replaceAll("\n", ",");
        jsonResponseBodyString = jsonResponseBodyString.substring(0, jsonResponseBodyString.length() - 1);

        // add json array syntax
        jsonResponseBodyString = "[" + jsonResponseBodyString + "]";

        return jsonResponseBodyString;
    }

    @Override
    public T convert(ResponseBody value) throws IOException {

        Reader reader = value.charStream();
        Scanner s = new Scanner(reader).useDelimiter("\\A");
        String result = s.hasNext() ? s.next() : "";

        if (result != null && result.length() > 0) {

            String jsonString = convertToJsonArray(result);
            T res = gson.fromJson(jsonString, type);
            return res;
        } else {
            return null;
        }
    }
}