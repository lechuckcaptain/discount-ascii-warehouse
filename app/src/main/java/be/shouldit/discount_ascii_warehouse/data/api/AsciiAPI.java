package be.shouldit.discount_ascii_warehouse.data.api;

import java.util.List;

import be.shouldit.discount_ascii_warehouse.data.Ascii;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by Marco on 08/06/16.
 */
public interface AsciiAPI {

    @GET("/api/search")
    Call<List<Ascii>> searchWarehouse(@Query("limit") int limit,
                                      @Query("skip") int skip,
                                      @Query("q") String query,
                                      @Query("onlyInStock") int onlyInStock,
                                      @Header("Cache-Control") String cacheControl);
}