package be.shouldit.discount_ascii_warehouse.data.api;


import android.content.Context;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import be.shouldit.discount_ascii_warehouse.data.Ascii;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import timber.log.Timber;

/**
 * Created by Marco on 08/06/16.
 */
public class AsciiProvider {

    private static final int CACHE_SIZE = 10 * 1024 * 1024; // 10 MiB

    private static final int CACHE_DURATION = 60 * 60 * 1; //1 hour (60sec * 60min * 1);
//    private static final int CACHE_DURATION = 60 * 2; //2 minutes (60sec * 10min);

    private static final String CACHE_CONNECTED = String.format("public,max-age=%d,s-maxage=%d", CACHE_DURATION, CACHE_DURATION);
    private static final String CACHE_DISCONNECTED = String.format("public, only-if-cached, max-stale=%d", CACHE_DURATION);
    private static final String CACHE_CONTROL = String.format("public, max-stale=%d", CACHE_DURATION);
    private final Context mContext;
    private final AsciiAPI mAsciiAPI;
    private final Retrofit mRetrofit;
    private final OkHttpClient mOkHttpClient;
//    private static final int CACHE_DURATION = 10; //10 seconds
    private final Cache mCache;
    private final HttpLoggingInterceptor mHttpLoggingInterceptor;

    public AsciiProvider(Context context) {

        mContext = context;

        File cacheDir = mContext.getCacheDir();

        mCache = new Cache(cacheDir, CACHE_SIZE);
        mHttpLoggingInterceptor = new HttpLoggingInterceptor();
        mHttpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        mOkHttpClient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .cache(mCache)
                .addInterceptor(mHttpLoggingInterceptor)
                .build();

        mRetrofit = new Retrofit.Builder()
                .client(mOkHttpClient)
                .addConverterFactory(ND_JSON_ConverterFactory.create())
                .baseUrl("http://74.50.59.155:5000/")
                .build();

        mAsciiAPI = mRetrofit.create(AsciiAPI.class);
    }

    public Call<List<Ascii>> searchAscii(int limit, int skip, String query, boolean onlyInStock) {

        Timber.d("Called searchAscii - Limit: '%d', Skip: '%d', Query: '%s', OnlyStock: '%b'", limit, skip, query, onlyInStock);
        return mAsciiAPI.searchWarehouse(limit, skip, query, onlyInStock ? 1:0, CACHE_CONTROL);
    }
}
