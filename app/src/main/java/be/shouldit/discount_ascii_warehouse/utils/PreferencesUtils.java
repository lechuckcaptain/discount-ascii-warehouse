package be.shouldit.discount_ascii_warehouse.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Marco on 09/07/16.
 */
public class PreferencesUtils {

    private final Context mContext;

    private final String APP_PREFERENCES = "APP_PREFERENCES";
    private final String SHOW_ONLY_IN_STOCK = "SHOW_ONLY_IN_STOCK";

    public PreferencesUtils(Context context) {
        mContext = context;
    }

    public boolean getStockPreference() {
        SharedPreferences settings = mContext.getSharedPreferences(APP_PREFERENCES, 0);
        boolean stockPreference = settings.getBoolean(SHOW_ONLY_IN_STOCK, false);
        return stockPreference;
    }

    public void setStockPreference(boolean stockPreference) {

        SharedPreferences settings = mContext.getSharedPreferences(APP_PREFERENCES, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(SHOW_ONLY_IN_STOCK, stockPreference);
        editor.apply();
    }
}
