package be.shouldit.discount_ascii_warehouse.ui;

import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import be.shouldit.discount_ascii_warehouse.App;
import be.shouldit.discount_ascii_warehouse.data.Ascii;
import be.shouldit.discount_ascii_warehouse.data.api.AsciiProvider;
import be.shouldit.discount_ascii_warehouse.ui.adapters.AsciiAdapter;
import okhttp3.HttpUrl;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public abstract class AsciiLoader extends Handler {

    public static final int PAGE_SIZE = 8;

    private String mQuery;
    private final RecyclerView mRecyclerView;
    private final AsciiAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private AsciiProvider mAsciiProvider;
    private boolean mDataAvailable;
    private int mNextPageToLoad;
    private int mRxTot;
    private List<Call<List<Ascii>>> mCalls;
    private EndlessRecyclerViewScrollListener mEndlessScrollListener;

    public AsciiLoader(RecyclerView recyclerView, AsciiAdapter adapter) {

        mAdapter = adapter;
        mRecyclerView = recyclerView;
        mCalls = new ArrayList<>();
        mLayoutManager =  (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mEndlessScrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore() {
                loadAscii();
            }
        };
        recyclerView.addOnScrollListener(mEndlessScrollListener);

        reset();
    }

    public void reset() {

        mQuery = "";
        mNextPageToLoad = 0;
        mDataAvailable = true;
        mRxTot = 0;

        for (Call<List<Ascii>> call : mCalls) {
            call.cancel();
        }

        mCalls.clear();
    }

    public void resetSearch() {
        resetSearch(mQuery, false);
    }

    public void resetSearch(String query) {
        resetSearch(query, false);
    }

    public void resetSearch(boolean forceReset) {
        resetSearch(mQuery, forceReset);
    }

    public void resetSearch(String query, boolean forceReset) {

        Timber.d("Reset search requested. Query: '%s', Force: %b", query, forceReset);

        if (forceReset || !mQuery.equals(query)) {

            reset();

            Timber.d("Start reset search");
            startLoadingProgress();

            mQuery = query;
            mAdapter.reset();
            loadAscii();
        } else {
            Timber.d("No need to reset search");
        }
    }

    @Override
    public void handleMessage(Message message) {
        load();
    }

    public void loadAscii() {
        sendEmptyMessage(0);
    }

    private void load() {

        if (mDataAvailable) {

            mEndlessScrollListener.setLoading(true);
            mAdapter.addLoadingProgress();
            mAsciiProvider = App.getAsciiProvider();
            boolean onlyStock = App.getPreferencesUtils().getStockPreference();

            Call<List<Ascii>> call = mAsciiProvider.searchAscii(PAGE_SIZE, mNextPageToLoad++ * PAGE_SIZE, mQuery, onlyStock);
            call.enqueue(new AsciiCallback());
            mCalls.add(call);
        }
        else {
//            Timber.d("Skip loading. No more data available for given query");
        }

    }

    public void setDataAvailable(boolean dataAvailable) {
        this.mDataAvailable = dataAvailable;
    }

    class AsciiCallback implements retrofit2.Callback<List<Ascii>> {

        @Override
        public void onResponse(Call<List<Ascii>> call, Response<List<Ascii>> response) {

            Request request = call.request();
            HttpUrl httpUrl = request.url();
            List<String> params = httpUrl.queryParameterValues("onlyInStock");

            List<Ascii> responseList = response.body();

            if (responseList != null) {

                int asciiNum = responseList.size();
                mDataAvailable = asciiNum == PAGE_SIZE;
                mRxTot += asciiNum;
                mAdapter.addAll(responseList);

                Timber.d("Received: '%d', '%d' tot RX", asciiNum, mRxTot);

            } else {

                mDataAvailable = false;
                Timber.d("Received from API empty response");
            }

            mAdapter.removeLoadingProgress();
            mEndlessScrollListener.setLoading(false);
            endLoadingProgress();

            if (mCalls.contains(call))
                mCalls.remove(call);
        }

        @Override
        public void onFailure(Call<List<Ascii>> call, Throwable t) {

            Timber.e(t, "Error during API call");

            if (!call.isCanceled()) {
                mAdapter.removeLoadingProgress();
                mEndlessScrollListener.setLoading(false);
                endLoadingProgress();
            }

            if (mCalls.contains(call))
                mCalls.remove(call);
        }
    }

    public abstract void startLoadingProgress();
    public abstract void endLoadingProgress();
}