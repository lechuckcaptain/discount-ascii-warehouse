package be.shouldit.discount_ascii_warehouse.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import be.shouldit.discount_ascii_warehouse.R;
import be.shouldit.discount_ascii_warehouse.ui.adapters.AsciiAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Marco on 06/03/16.
 */
public class ProgressViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.progress)
    ProgressBar progressBar;

    public ProgressViewHolder(AsciiAdapter asciiAdapter, View view) {

        super(view);
        ButterKnife.bind(this, view);
    }
}
