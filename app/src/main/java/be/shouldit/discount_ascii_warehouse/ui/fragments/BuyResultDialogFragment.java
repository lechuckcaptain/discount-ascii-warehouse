package be.shouldit.discount_ascii_warehouse.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import be.shouldit.discount_ascii_warehouse.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mpagliar on 12/08/2016.
 */
public class BuyResultDialogFragment extends DialogFragment
{
    @BindView(R.id.message_dialog)
    TextView messageDialog;

    private String mMessage;
    private String mTitle;

    public BuyResultDialogFragment() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_view_ascii_buy_confirmation, container);
        ButterKnife.bind(this, view);

        messageDialog.setText(mMessage);
        getDialog().setTitle(mTitle);
        return view;
    }

    public void setMessage(String text)
    {
        mMessage = text;
    }

    public void setTitle(String text)
    {
        mTitle = text;
    }
}
