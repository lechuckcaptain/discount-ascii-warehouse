package be.shouldit.discount_ascii_warehouse.ui.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import be.shouldit.discount_ascii_warehouse.R;
import be.shouldit.discount_ascii_warehouse.data.Ascii;
import be.shouldit.discount_ascii_warehouse.ui.FragmentsUtils;
import be.shouldit.discount_ascii_warehouse.ui.OnAsciiItemSelected;
import be.shouldit.discount_ascii_warehouse.ui.fragments.AsciiListFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Application Main Activity.
 */
public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnAsciiItemSelected
{

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    private AsciiListFragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(DRAWER_LAYOUT_ACTIVITY);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        mFragment = AsciiListFragment.newInstance();
        FragmentManager fm = getSupportFragmentManager();
        FragmentsUtils.changeFragment(fm, R.id.fragment_container, mFragment, false);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayUseLogoEnabled(false);
        }

        Timber.d("Application ready");
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {

            this.drawer.openDrawer(GravityCompat.START);  // OPEN DRAWER);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_sit) {
            mFragment.setQuery("sit");
        } else if (id == R.id.nav_ipsum) {
            mFragment.setQuery("ipsum");
        } else if (id == R.id.nav_lorem) {
            mFragment.setQuery("lorem");
        } else if (id == R.id.nav_nose) {
            mFragment.setQuery("nose");
        } else if (id == R.id.nav_consectetur) {
            mFragment.setQuery("consectetur");
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onAsciiItemSelected(Ascii asciiItem)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_view_ascii_buy_confirmation, null);
        TextView message = ButterKnife.findById(alertLayout,R.id.message_dialog);
        TextView ascii = ButterKnife.findById(alertLayout,R.id.message_ascii);

        if (asciiItem.getStock() > 0)
        {
            builder.setTitle(getString(R.string.congratulations));
            message.setText(getString(R.string.you_just_bought_ascii_art));
        }
        else
        {
            builder.setTitle(getString(R.string.Sorry));
            message.setText(getString(R.string.we_are_out_of_stock_for_ascii_art));
        }

        ascii.setText(asciiItem.getFace());
        builder.setView(alertLayout);
        builder.setPositiveButton("OK", null);
        builder.show();
    }
}
