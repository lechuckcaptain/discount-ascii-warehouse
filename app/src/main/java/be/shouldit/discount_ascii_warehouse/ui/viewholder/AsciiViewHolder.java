package be.shouldit.discount_ascii_warehouse.ui.viewholder;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

import be.shouldit.discount_ascii_warehouse.R;
import be.shouldit.discount_ascii_warehouse.data.Ascii;
import be.shouldit.discount_ascii_warehouse.ui.adapters.AsciiAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Marco on 06/03/16.
 */
public class AsciiViewHolder extends RecyclerView.ViewHolder {

    ;
    @BindView(R.id.price)
    TextView priceTextView;

    @BindView(R.id.face)
    TextView faceTextView;

    @BindView(R.id.stock)
    TextView stockTextView;

    @BindView(R.id.tags)
    TextView tagsTextView;

    private Ascii mAscii;
    private Context mContext;
    private final AsciiAdapter mAsciiAdapter;

//    @BindView(R.id.stock) TextView stockTextView;
//    @BindView(R.id.type) TextView typeTextView;
//    @BindView(R.id.id) TextView idTextView;

    public AsciiViewHolder(AsciiAdapter asciiAdapter, View view) {

        super(view);

        mAsciiAdapter = asciiAdapter;
        mContext = view.getContext();
        ButterKnife.bind(this, view);
    }

    @Override
    public String toString() {

        return super.toString() + " '" + faceTextView.getText() + "'";
    }

    private void setFace(String text) {

        this.faceTextView.setText(text);
    }

    private void setStock(int stock) {

//        UIUtils.setVisible(this.stockTextView, stock == 1);

        Resources res = this.stockTextView.getResources();
        String text = null;
        if (stock != 1)
            text = String.format(res.getString(R.string.items), stock);
        else
            text = res.getString(R.string.only_1_item_in_stock);

        this.stockTextView.setText(text);
    }

//    public void setType(String type) {
//
//        this.typeTextView.setText(type);
//    }

    //    public void setId(String id) {
//
//        this.idTextView.setText(id);
//    }
//
    private void setPrice(Integer price) {

        Locale defaultLocale = Locale.getDefault();
//        Timber.d("Got default locale: %s", defaultLocale.toString());
        Currency defaultCurrency = Currency.getInstance(defaultLocale);
//        Timber.d("Got default currency: %s", defaultCurrency.toString());

        NumberFormat format = NumberFormat.getCurrencyInstance(defaultLocale);
        format.setCurrency(defaultCurrency);
        String result = format.format(price);

        this.priceTextView.setText(result);
    }

    //    public void setStock(Integer stock) {
//
//        this.stockTextView.setText(String.valueOf(stock));
//    }
//
    public void setTags(List<String> tags) {

        this.tagsTextView.setText(TextUtils.join(", ", tags));
    }

    @OnClick(R.id.buy)
    public void buyItemClick() {

        mAsciiAdapter.asciiItemSelected(mAscii);
    }

    public void setAscii(Ascii ascii) {

        mAscii = ascii;

        setFace(ascii.getFace());
        setPrice(ascii.getPrice());
        setStock(ascii.getStock());
        setTags(ascii.getTags());
    }
}
