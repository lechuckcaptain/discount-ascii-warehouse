package be.shouldit.discount_ascii_warehouse.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import be.shouldit.discount_ascii_warehouse.R;
import be.shouldit.discount_ascii_warehouse.data.Ascii;
import be.shouldit.discount_ascii_warehouse.ui.OnAsciiItemSelected;
import be.shouldit.discount_ascii_warehouse.ui.viewholder.AsciiViewHolder;
import be.shouldit.discount_ascii_warehouse.ui.viewholder.ProgressViewHolder;
import timber.log.Timber;

/**
 * Created by Marco on 11/06/16.
 */
public class AsciiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int ITEM_VIEW_TYPE_ASCII = 1;
    public static final int ITEM_VIEW_TYPE_LOADING_FOOTER = 0;

    private final List<Ascii> asciiList;
    private final OnAsciiItemSelected onAsciiItemSelected;

    public AsciiAdapter(OnAsciiItemSelected onAsciiItemSelected) {

        this.onAsciiItemSelected = onAsciiItemSelected;
        this.asciiList = new ArrayList<>();
    }

    public void addAll(List<Ascii> list) {

        for (Ascii ascii : list) {
            if (!asciiList.contains(ascii)) {
                add(ascii);
            }
        }
    }

    public void addLoadingProgress() {
        add(null);
    }

    public void removeLoadingProgress() {
        remove(null);
    }

    public void add(Ascii ascii) {

        int position = getItemCount();

        if (!asciiList.contains(ascii)) {
            asciiList.add(ascii);
            notifyItemInserted(position);
            Timber.d("AsciiAdapter size: %d items", getItemCount());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return asciiList.get(position) != null ? ITEM_VIEW_TYPE_ASCII : ITEM_VIEW_TYPE_LOADING_FOOTER;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == ITEM_VIEW_TYPE_ASCII) {

            View itemView = LayoutInflater.
                    from(parent.getContext()).
                    inflate(R.layout.fragment_ascii_list_item, parent, false);

            return new AsciiViewHolder(this, itemView);

        } else if (viewType == ITEM_VIEW_TYPE_LOADING_FOOTER) {

            View itemView = LayoutInflater.
                    from(parent.getContext()).
                    inflate(R.layout.fragment_progress_list_item, parent, false);

            return new ProgressViewHolder(this, itemView);

        } else {
            throw new IllegalStateException(String.format("Invalid type: '%d'", viewType));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (getItemViewType(position) == ITEM_VIEW_TYPE_ASCII) {

            AsciiViewHolder asciiViewHolder = (AsciiViewHolder) holder;
            Ascii ascii = asciiList.get(position);
            Timber.d("Setting Ascii viewholder: %s", ascii.toString());
            asciiViewHolder.setAscii(ascii);
        } else {

            // Do something here for ProgressViewHolder
        }
    }

    @Override
    public int getItemCount() {

        return asciiList.size();
    }

    public void remove(Ascii ascii) {

        int indexOfItem = asciiList.indexOf(ascii);

        if (indexOfItem != -1) {
            asciiList.remove(ascii);
            notifyItemRemoved(indexOfItem);
            Timber.d("AsciiAdapter size: %d items", getItemCount());
        }
    }

    public void reset() {
        asciiList.clear();
        notifyDataSetChanged();
    }

    public void asciiItemSelected(Ascii ascii)
    {
        onAsciiItemSelected.onAsciiItemSelected(ascii);
    }
}
