package be.shouldit.discount_ascii_warehouse.ui.fragments;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import be.shouldit.discount_ascii_warehouse.App;
import be.shouldit.discount_ascii_warehouse.R;
import be.shouldit.discount_ascii_warehouse.ui.AsciiLoader;
import be.shouldit.discount_ascii_warehouse.ui.OnAsciiItemSelected;
import be.shouldit.discount_ascii_warehouse.ui.adapters.AsciiAdapter;
import be.shouldit.discount_ascii_warehouse.utils.UIUtils;
import butterknife.BindView;
import timber.log.Timber;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class AsciiListFragment extends BaseFragment {

    public static final int COLUMNS_PORTRAIT = 2;
    public static final int COLUMNS_LANDSCAPE = 4;

    @BindView(R.id.list)
    RecyclerView mRecyclerView;

    @BindView(R.id.no_data_text)
    TextView noDataText;

    @BindView(R.id.fragment_container)
    RelativeLayout fragmentContainer;

    private GridLayoutManager mLayoutManager;
    private AsciiAdapter mAdapter;

    private MenuItem mShowOnlyInStockMenuItem;
    private String mSearchViewText;

    private AsciiLoader mLoader;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AsciiListFragment() {


        mSearchViewText = "";
    }

    public static AsciiListFragment newInstance() {

        AsciiListFragment fragment = new AsciiListFragment();
        return fragment;
    }

    private static int getColumnsCount(Context context) {

        int columns;

        if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            columns = COLUMNS_PORTRAIT;
        } else {
            columns = COLUMNS_LANDSCAPE;
        }

        return columns;
    }

    @Override
    protected void startProgress() {

        super.startProgress();

        UIUtils.setVisible(mRecyclerView, false);
        UIUtils.setVisible(noDataText, false);
    }

    @Override
    public void endProgress() {

        super.endProgress();

        boolean dataAvailable = mAdapter.getItemCount() > 0;

        UIUtils.setVisible(mRecyclerView, dataAvailable);
        UIUtils.setVisible(noDataText, !dataAvailable);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ascii_list, container, false);
        butterknifeBind(this, view);

        setupUI();
        getActivity().invalidateOptionsMenu();

        return view;
    }

    private void setupUI() {

        Context context = mRecyclerView.getContext();

        int columns;
        columns = getColumnsCount(getActivity());

        mLayoutManager = new GridLayoutManager(context, columns);
        mAdapter = new AsciiAdapter((OnAsciiItemSelected) getActivity());

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {

            @Override
            public int getSpanSize(int position) {

                return mAdapter.getItemViewType(position) == AsciiAdapter.ITEM_VIEW_TYPE_LOADING_FOOTER ? getColumnsCount(getActivity()) : 1;
            }
        });

        fragmentContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search();
            }
        });

        mLoader = new AsciiLoader(mRecyclerView, mAdapter) {

            @Override
            public void startLoadingProgress() {
                startProgress();
            }

            @Override
            public void endLoadingProgress() {
                endProgress();
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        search();
    }

    public void setQuery(String query) {

        mSearchViewText = query;
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

        MenuItem searchItem = menu.findItem(R.id.search);
        if (searchItem != null) {

            SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            if (searchView != null) {

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                    @Override
                    public boolean onQueryTextSubmit(String query) {

                        Timber.d("Text entered: '%s'", query);
                        resetSearch(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {

                        Timber.d("Text changed: '%s'", newText);
                        resetSearch(newText);
                        return false;
                    }
                });

                if (TextUtils.isEmpty(mSearchViewText)) {
                    MenuItemCompat.collapseActionView(searchItem);
                    searchView.setQuery("", true);
                } else {
                    MenuItemCompat.expandActionView(searchItem);
                    searchView.setQuery(mSearchViewText, true);
                }
            }

            MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {

                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {

                    resetSearch();
                    return true;
                }
            });
        }

        mShowOnlyInStockMenuItem = menu.findItem(R.id.action_show_only_in_stock);
        mShowOnlyInStockMenuItem.setChecked(App.getPreferencesUtils().getStockPreference());
    }

    public void search() {

        startProgress();
        mLoader.setDataAvailable(true);
        mLoader.loadAscii();
    }

    public void resetSearch() {
        mLoader.resetSearch();
    }

    public void resetSearch(String query) {
        mLoader.resetSearch(query);
    }

    public void resetSearch(boolean forceReset) {
        mLoader.resetSearch(forceReset);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_show_only_in_stock:
                mShowOnlyInStockMenuItem.setChecked(!mShowOnlyInStockMenuItem.isChecked());
                App.getPreferencesUtils().setStockPreference(mShowOnlyInStockMenuItem.isChecked());
                resetSearch(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}


