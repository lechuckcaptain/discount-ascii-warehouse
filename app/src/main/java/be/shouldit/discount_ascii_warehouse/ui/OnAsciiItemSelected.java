package be.shouldit.discount_ascii_warehouse.ui;

import be.shouldit.discount_ascii_warehouse.data.Ascii;

/**
 * Created by mpagliar on 12/08/2016.
 */
public interface OnAsciiItemSelected
{
    void onAsciiItemSelected(Ascii asciiItem);
}
