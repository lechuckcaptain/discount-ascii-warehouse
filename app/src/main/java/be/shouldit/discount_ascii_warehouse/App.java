package be.shouldit.discount_ascii_warehouse;

import android.app.Application;

import be.shouldit.discount_ascii_warehouse.data.api.AsciiProvider;
import be.shouldit.discount_ascii_warehouse.ui.activities.BaseActivity;
import be.shouldit.discount_ascii_warehouse.utils.CrashlyticsTree;
import be.shouldit.discount_ascii_warehouse.utils.InitUtils;
import be.shouldit.discount_ascii_warehouse.utils.PreferencesUtils;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by mpagliar on 26/10/2015.
 */
public class App extends Application {

    private static final Object lock = new Object();
    private static BaseActivity mCurrentActivity;
    private static App instance;

    private boolean fabricSetupDone;
    private AsciiProvider asciiProvider;
    private PreferencesUtils preferencesUtils;

    public static App getInstance() {

        return instance;
    }

    public static AsciiProvider getAsciiProvider() {

        if (getInstance().asciiProvider == null) {

            getInstance().asciiProvider = new AsciiProvider(getInstance());
        }

        return getInstance().asciiProvider;
    }

    public static PreferencesUtils getPreferencesUtils() {
        if (getInstance().preferencesUtils == null) {

            getInstance().preferencesUtils = new PreferencesUtils(getInstance());
        }

        return getInstance().preferencesUtils;
    }

    public static void setCurrentActivity(BaseActivity activity) {

        synchronized (lock) {
            mCurrentActivity = activity;
        }
    }

    @Override
    public void onCreate() {

        super.onCreate();

        instance = this;

        Timber.plant(new CrashlyticsTree());

        fabricSetupDone = InitUtils.setupFabric(App.this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/unifont.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
