package be.shouldit.discount_ascii_warehouse.ui.activities;

import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import be.shouldit.discount_ascii_warehouse.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AsciiSearchTest {

    public static RecyclerViewMatcher withRecyclerView(final int recyclerViewId) {
        return new RecyclerViewMatcher(recyclerViewId);
    }

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void asciiSearchTest() throws InterruptedException {

        String tag = "nose";

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.search), withContentDescription("Search ascii"), isDisplayed()));
        actionMenuItemView.perform(click());

        ViewInteraction searchAutoComplete = onView(
                        allOf(withId(R.id.search_src_text),
                        withParent(allOf(withId(R.id.search_plate),
                        withParent(withId(R.id.search_edit_frame)))),
                        isDisplayed()));

        searchAutoComplete.perform(replaceText(tag));

        // "nose" tag is referred only by two Ascii
        onView(withRecyclerView(R.id.list)
                .atPositionOnView(0,R.id.tags))
                .check(matches(withText(containsString(tag))));

        onView(withRecyclerView(R.id.list)
                .atPositionOnView(1,R.id.tags))
                .check(matches(withText(containsString(tag))));

        Thread.sleep(100);
    }
}
