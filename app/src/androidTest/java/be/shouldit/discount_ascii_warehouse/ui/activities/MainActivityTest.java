package be.shouldit.discount_ascii_warehouse.ui.activities;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import be.shouldit.discount_ascii_warehouse.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void consumeAsciiTest() throws IOException, InterruptedException {

        for (int i = 0; i < 100; i++) {

            onView(withId(R.id.list)).perform(RecyclerViewActions.scrollToPosition(i));

            Thread.sleep(100);
        }
    }
}
