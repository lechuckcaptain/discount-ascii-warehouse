-keepattributes *Annotation*,EnclosingMethod,Signature
-keep class com.fasterxml.** { *; }
-keep class com.fasterxml.jackson.annotation.** {*;}
-keep class com.fasterxml.jackson.core.** {*;}
-keep class org.codehaus.** { *; }
-dontwarn com.fasterxml.jackson.databind.**
-keepclassmembers public final enum org.codehaus.jackson.annotate.JsonAutoDetect$Visibility {
 public static final org.codehaus.jackson.annotate.JsonAutoDetect$Visibility *; }
-keepnames class be.shouldit.** { *; }