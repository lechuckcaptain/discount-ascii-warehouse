Discount ASCII warehouse 
========================

Android App that reads Ascii arts from a rest API.


Requirements
------------

Create a very simple app, called "Discount Ascii Warehouse". 

You can get search results from the API with a request like this:

```
curl http://74.50.59.155:5000/api/search
```

The API also accepts some parameters:

```
GET /api/search
```

Parameters:

* limit (int) - Max number of search results to return
* skip (int) - Number of results to skip before sending back search results
* q (string) - Search query. Tags separated by spaces.
* onlyInStock (bool) - When flag is set, only return products that are currently in stock.

Response Type: NDJSON

The app should keep loading products from the API until it has enough to fill the screen, and then wait until the user has swiped to the bottom to load more.  The app should cache API requests for 1 hour.